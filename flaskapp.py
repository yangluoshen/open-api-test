# coding: utf-8

from flask import Flask, Blueprint, request, jsonify
from flask_restful import Api, Resource, url_for
import requests
import logging
logging.basicConfig(filename='debug.log',level=logging.DEBUG)

app = Flask(__name__)

blueprint = Blueprint('hookproxy',  __name__)


oapi_url = 'https://test-api.zjurl.cn/open-apis/fucntionbot/api/transform'
config = {
        'jira': {
            'token': '456-123',
            'bot_id': '1514457241362023',
            'access_token': 'efg-abc',
            'chat_id': '1511438431177650',
        },
        'gitlab': {
            'token': '123-456',
            'bot_id': '1511492395349197',
            'access_token': 'abc-efg',
            'chat_id': '1511438431177650',
        },
        'jenkins': {
            'token': '456-789',
            'bot_id': '1514458518626709',
            'access_token': 'hij-klm',
            'chat_id': '1511438431177650',
        },
}


@blueprint.route('/<string:site>', methods=['POST'])
def proxy(site):

    data = request.get_json()
    resp = route(site, data)
    if not resp:
        return jsonify(ok=False, error='unknow site')

    logging.info(resp.json())
    if resp.status_code != 200:
        return jsonify(ok=False, error='invalid data')

    return jsonify(ok=True)


def route(site, data):

    c = config.get(site)
    if not c:
        logging.warning('unknown {site}')
        return

    data_packed = {
        'token': c.get('token'),
        'type': 'event_callback',
        'event':{
            'type': 'receive_incoming_webhook_data',
            'bot_id': c.get('bot_id'),
            'bot_type': site,
            'incoming_webhook_data':{
                'access_token': c.get('access_token'),
                'chat_id': c.get('chat_id'),
                'data': data,
            }
        }
    }

    return requests.post(url=oapi_url, json=data_packed)

app.register_blueprint(blueprint)

if __name__ == '__main__':

    app.run(host='127.0.0.1', port=8091)
